window.YTD.ad_impressions.part0 = [ {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1103589098021965825",
            "tweetText" : "Our new Mind Mapping tool maintains a good balance of simplicity and function and helps you bring your ideas to life. Give it a try and don't hesitate to share your feedback with us! https://t.co/bXOCWRQ6rl https://t.co/TzHWmB4Xnc",
            "urls" : [ "https://t.co/bXOCWRQ6rl" ],
            "mediaUrls" : [ "https://t.co/TzHWmB4Xnc" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-03-08 05:59:01"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Huawei",
            "screenName" : "@Huawei"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nokia"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Facebook"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Tesla"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Microsoft"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@YouTube"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Cisco"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Windows"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@intel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Sony"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Huawei Cloud"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#ai #artificialintelligence"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Russia"
          } ],
          "impressionTime" : "2020-03-08 05:59:00"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "peter",
            "screenName" : "@polargrid3d"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SheepItRF"
          } ],
          "impressionTime" : "2020-03-08 16:26:57"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Точка",
            "screenName" : "@bankTochka"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Inc"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Entrepreneur"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@business"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WSJ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Russia"
          } ],
          "impressionTime" : "2020-03-08 11:03:17"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Точка",
            "screenName" : "@bankTochka"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Inc"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Entrepreneur"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@business"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WSJ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Russia"
          } ],
          "impressionTime" : "2020-03-08 11:43:52"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1172383036949725184",
            "tweetText" : "Be more productive - have your ideas, sketches, and concepts on any device, anytime, anywhere. Get started free. https://t.co/iKjNjMgKxn https://t.co/EmnofYnQOz",
            "urls" : [ "https://t.co/iKjNjMgKxn" ],
            "mediaUrls" : [ "https://t.co/EmnofYnQOz" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Pinterest"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-03-08 11:01:32"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245697675967770624",
            "tweetText" : "#Bitcoin prices are lower than at the start of the year and with the halving fast approaching, what do you think is the best way to trade? #cryptotwitter",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swissquote",
            "screenName" : "@Swissquote"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Bitcoin cryptocurrency"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cryptocurrencies"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 20:17:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "SGKB News",
            "screenName" : "@sgkb_news"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Financial news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Financial planning"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FortuneMagazine"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TIME"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Entrepreneur"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tagesanzeiger"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@handelsblatt"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@businessinsider"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TEDTalks"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MariaBartiromo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MarketWatch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SRF"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 20:17:36"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1241359072282189826",
            "tweetText" : "❓Single-Page-Applications für's Web mit C# und .NET?\nℹ️Zwei Welten kommen zusammen: es gibt einiges zu beachten. 📄 Laden Sie sich kostenlos das #Whitepaper von @christianweyer zu Microsoft https://t.co/SF7ZzsMbtt Core #Blazor #WebAssembly herunter.\nhttps://t.co/46FgEQaisS https://t.co/SJIItLtoWO",
            "urls" : [ "https://t.co/SF7ZzsMbtt", "https://t.co/46FgEQaisS" ],
            "mediaUrls" : [ "https://t.co/SJIItLtoWO" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Thinktecture",
            "screenName" : "@thinktecture"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 20:21:52"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244949174799478788",
            "tweetText" : "Benötigen Sie eine Sicherungsstrategie für Ihre Windows-Server und -Desktops? \nLaden Sie kostenlos unseren Leitfaden zur vollständigen Sicherung und System-Wiederherstellung herunter!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "MSP360",
            "screenName" : "@msp360"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer programming"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-04-03 01:29:47"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1231881545398525952",
            "tweetText" : "Die digitale Telefonie bietet zahlreiche Vorteile - gerade auch für #KMU:\n#VoIP Lösungen sind flexibel, kostengünstig und einfach zu bedienen.  \n\nLesen Sie hier anhand eines Kundenbeispiels, welche Vorteile VoIP in der Praxis bringen:\n▶ https://t.co/TdM3xMUiWr https://t.co/PPOX0wwnFR",
            "urls" : [ "https://t.co/TdM3xMUiWr" ],
            "mediaUrls" : [ "https://t.co/PPOX0wwnFR" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infortix",
            "screenName" : "@infortix_ch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Enterprise software"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "B2B"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "KMU"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 01:30:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "DuckDuckGo",
            "screenName" : "@DuckDuckGo"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BlackHatEvents"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@torproject"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EFF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@thegrugq"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kevinmitnick"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheHackersNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ioerror"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mikko"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@schneierblog"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 01:30:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "DuckDuckGo",
            "screenName" : "@DuckDuckGo"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BlackHatEvents"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@torproject"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EFF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@thegrugq"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kevinmitnick"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheHackersNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ioerror"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mikko"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@schneierblog"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 01:29:52"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1208154831757537281",
            "tweetText" : "Build it fast, build it right, never pay for what you don’t use. Try FaunaDB, the database built for serverless. https://t.co/kLVmpakm7N",
            "urls" : [ "https://t.co/kLVmpakm7N" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Fauna",
            "screenName" : "@fauna"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "netlify"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "JAMstack"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "hasura"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "javascript"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "frontend"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "zeit"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 01:30:02"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245471249368641536",
            "tweetText" : "Pas envie de conseils aléatoires ? Prends ton pilier 3a en main. #cesttoiquidécides",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "promotedTrendInfo" : {
            "trendId" : "71985",
            "name" : "#franklyzkb20200403",
            "description" : ""
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 01:05:02"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240578761495425040",
            "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 02:11:16"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242874852836589575",
            "tweetText" : "Für eure Service-Anliegen sind die Swisscom Shops an den wichtigsten Standorten von Mo–Sa von 10 bis 15 Uhr geöffnet.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 02:19:34"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242879802635423744",
            "tweetText" : "Zur Ablenkung für unsere Swisscom TV-Kunden gibt es im April die besten Filme gratis auf Teleclub Movie.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 22:36:44"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243185750440583168",
            "tweetText" : "Strom klug nutzen dank intelligenter Stromnetze: Ein Forschungsteam der Hochschule Luzern – Informatik hat dazu neuartige Lösungsansätze entwickelt. #hsluinformatik #smartgrid #informatik #predictiveanalytics",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Hochschule Luzern",
            "screenName" : "@hslu"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Informatik"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Strom"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 22:37:03"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237645839138525184",
            "tweetText" : "In Immobilien investieren? Valuu verkuppelt Sie mit der besten Hypothek dazu. #valuu",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "promotedTrendInfo" : {
            "trendId" : "72405",
            "name" : "#valuu",
            "description" : ""
          },
          "advertiserInfo" : {
            "advertiserName" : "Valuu",
            "screenName" : "@valuuapp"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 22:35:57"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243235226458808320",
            "tweetText" : "Learn how you can improve the security of your infrastructure and applications with the DevOps security checklist. ✅ #DevOps #DevSecOps",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlabstatus"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DevOps"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "gitlab"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 22:35:27"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1103589098021965825",
            "tweetText" : "Our new Mind Mapping tool maintains a good balance of simplicity and function and helps you bring your ideas to life. Give it a try and don't hesitate to share your feedback with us! https://t.co/bXOCWRQ6rl https://t.co/TzHWmB4Xnc",
            "urls" : [ "https://t.co/bXOCWRQ6rl" ],
            "mediaUrls" : [ "https://t.co/TzHWmB4Xnc" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Business news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Design"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Graphics software"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 22:35:05"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242877792880136194",
            "tweetText" : "With Bitpanda, you can invest in #Bitcoin, gold and a list of over 30 digital assets quickly, safely and from just €1.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Bitpanda",
            "screenName" : "@bitpanda"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ExpansionMx"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@lajornadaonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Pajaropolitico"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@VitalikButerin"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#cybersecurity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "cybersecurity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "crypto"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#crypto"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 22:39:05"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240508573916037120",
            "tweetText" : "Collaborate on an online whiteboard just like you would in Google Docs. Free forever. Works for teams of all sizes. Sign up and try! https://t.co/zbpe52mEgL https://t.co/gjro73DylN",
            "urls" : [ "https://t.co/zbpe52mEgL" ],
            "mediaUrls" : [ "https://t.co/gjro73DylN" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Enterprise software"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Graphics software"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Google"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "GitHub"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Microsoft Office"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 to 54"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 21:30:29"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1232967401236574214",
            "tweetText" : "⚠ Eines der grössten IT-Risiken: mangelhaft unterhaltene #Firewall! \n🤝Daher bieten wir #ManagedFirewall Lösungen an. Hardware &amp; Software werden von uns zur Verfügung gestellt &amp; laufend betreut, damit Sie sich auf Ihr Geschäft konzentrieren könnnen.\n\nℹ https://t.co/pNP3C4P4ZF https://t.co/GeUff0RPyW",
            "urls" : [ "https://t.co/pNP3C4P4ZF" ],
            "mediaUrls" : [ "https://t.co/GeUff0RPyW" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infortix",
            "screenName" : "@infortix_ch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Enterprise software"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "B2B"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "KMU"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:28:33"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1104028125376270342",
            "tweetText" : "Plötzlich PHP 7 - Wir retten Ihre Webseite! Migrieren Sie jetzt, Ihre Webseite auf WordPress. https://t.co/7mXXC5OTPU https://t.co/ZHmKAbQTGs",
            "urls" : [ "https://t.co/7mXXC5OTPU" ],
            "mediaUrls" : [ "https://t.co/ZHmKAbQTGs" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "OVA oliver von arx + partner",
            "screenName" : "@Social_Media_CH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Business news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Government"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Green solutions"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Leadership"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:31:25"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243538916218724354",
            "tweetText" : "Wie können Sie langfristige Beziehungen zu Ihren Kunden aufbauen? \nLaden Sie unseren Onboarding-Leitfaden für MSPs kostenlos herunter!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "MSP360",
            "screenName" : "@msp360"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer programming"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-04-03 21:30:25"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1141071667759910912",
            "tweetText" : "How mature is your monitoring? Discover your current maturity level and what steps you need to take to improve 💪🏼",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "StackState",
            "screenName" : "@gostackstate"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@zabbix"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@puppetize"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:33:41"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243585438222295040",
            "tweetText" : "Aufgrund der Schliessungen stehen Lehrpersonen und Bildungseinrichtungen vor der grossen Herausforderung. Microsoft Schweiz möchte Sie unterstützen, neue Wege zum Lernen zu ermöglichen. #Fernunterricht #Teams #Office365 https://t.co/VvrVqDCFz5",
            "urls" : [ "https://t.co/VvrVqDCFz5" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Microsoft Education Schweiz",
            "screenName" : "@PiLSchweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Bildung_Schweiz"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:27:08"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244928479679971329",
            "tweetText" : "🚨The #Geneva Tech Startup Financial Survival Kit\n#Startups can be particularly vulnerable in this crisis. Here is an actionable list of the measures that your company should be looking at right now, in order of recommended implementation 👉 https://t.co/9j5hHPI6IB https://t.co/F9h8XwLDEn",
            "urls" : [ "https://t.co/9j5hHPI6IB" ],
            "mediaUrls" : [ "https://t.co/F9h8XwLDEn" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Fongit",
            "screenName" : "@Fongit1"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WIRED"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EPFL_Park"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TechCrunch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UNIGEnews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Innosuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@platinn_CH"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:33:28"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240578761495425040",
            "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:29:06"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1227090844534136832",
            "tweetText" : "Collaborate, visualize, collect ideas, and share work using Miro, infinite whiteboard platform for cross-functional team collaboration. Check it out! https://t.co/q8LFi613tr https://t.co/9bQbBSI7na",
            "urls" : [ "https://t.co/q8LFi613tr" ],
            "mediaUrls" : [ "https://t.co/9bQbBSI7na" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Business news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Financial news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Financial planning"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Computer networking"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Graphics software"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Web design"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Government"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Leadership"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Microsoft Office"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 21:28:28"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Financial news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Financial planning"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Green solutions"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Investors and patents"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business and finance news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business & finance"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:16:23"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1231000165739155456",
            "tweetText" : "Learn the latest best practices for security engineers, WebOps, and CISOs to prioritize and ramp up their security efforts with this checklist. ✅",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlabstatus"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "gitlab"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 21:31:15"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1181307044550381569",
            "tweetText" : "Correlate logs with metrics and traces, easily investigate trends with the Log Patterns view, and now, re-index archived logs to provide historical context. Learn more here: https://t.co/MnoLgq4YuK",
            "urls" : [ "https://t.co/MnoLgq4YuK" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Datadog, Inc.",
            "screenName" : "@datadoghq"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer programming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Enterprise software"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Open source"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DevOps"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#debugging"
          }, {
            "targetingType" : "Tailored audiences (lists)",
            "targetingValue" : "AWS Customers v2"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 21:33:31"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514970320207872",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 21:35:21"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1205193106993238017",
            "tweetText" : "Account Takeover (ATO) attacks are on the rise. Learn how to detect these attacks and defend your users --&gt; https://t.co/cwGs74WvAQ https://t.co/DXD2HVY9bh",
            "urls" : [ "https://t.co/cwGs74WvAQ" ],
            "mediaUrls" : [ "https://t.co/DXD2HVY9bh" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Signal Sciences",
            "screenName" : "@signalsciences"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@thegrugq"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:28:08"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245267569755463681",
            "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
            "urls" : [ "https://t.co/I4grLgGEqj" ],
            "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Datenschutz"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:29:59"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237645336203624449",
            "tweetText" : "Unsere Hypothekarexperten verkuppeln Sie mit Ihrer Traum-Hypothek. #valuu",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Valuu",
            "screenName" : "@valuuapp"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:29:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242885285454675968",
            "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:16:24"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244950281584087041",
            "tweetText" : "Dein nächstes Abenteuer wartet im PS Store mit tollen Frühlingsrabatten auf dich",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PlayStation Europe",
            "screenName" : "@PlayStationEU"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Console gaming"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 21:27:04"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242874852836589575",
            "tweetText" : "Für eure Service-Anliegen sind die Swisscom Shops an den wichtigsten Standorten von Mo–Sa von 10 bis 15 Uhr geöffnet.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-03 14:40:23"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "DuckDuckGo",
            "screenName" : "@DuckDuckGo"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BlackHatEvents"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@torproject"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EFF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@thegrugq"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@briankrebs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kevinmitnick"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TheHackersNews"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ioerror"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@mikko"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@schneierblog"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-03 14:39:59"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514947989671937",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-04 05:33:04"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242874852836589575",
            "tweetText" : "Für eure Service-Anliegen sind die Swisscom Shops an den wichtigsten Standorten von Mo–Sa von 10 bis 15 Uhr geöffnet.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-04 05:33:03"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Financial news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Financial planning"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Green solutions"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business & finance"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business and finance news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Investors and patents"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-04 16:57:28"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Financial news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Financial planning"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Green solutions"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business and finance news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Investors and patents"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business & finance"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-06 19:59:09"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514899310542848",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-06 19:59:11"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244949117362671616",
            "tweetText" : "Wie sichern Sie Ihren Windows-Computer? \nLaden Sie unseren neuen Leitfaden zur vollständigen Systemsicherung und -wiederherstellung kostenlos herunter!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "MSP360",
            "screenName" : "@msp360"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer programming"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-04-06 20:06:51"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245376969421848583",
            "tweetText" : "Es gibt viele Wege smart verbunden zu sein: Entdecke die Apple Watch mit Multi Device.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iPhone"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-04-06 20:05:24"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "ProfileTweets",
          "promotedTweetInfo" : {
            "tweetId" : "1244514899310542848",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-08 08:50:26"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-08 08:50:42"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-08 08:50:26"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "ProfileAccountsSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Polizei.news",
            "screenName" : "@polizeiCH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-08 08:50:26"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "ClusterFollow",
          "advertiserInfo" : {
            "advertiserName" : "Polizei.news",
            "screenName" : "@polizeiCH"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-08 08:50:28"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-08 06:16:51"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "ProfileAccountsSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@federalreserve"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Investors and patents"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business & finance"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business and finance news"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-08 06:16:51"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "ProfileTweets",
          "promotedTweetInfo" : {
            "tweetId" : "1244514844566597635",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-08 06:16:17"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514899310542848",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-08 06:23:44"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-08 06:16:17"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "ProfileAccountsSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@federalreserve"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business & finance"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Investors and patents"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business and finance news"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-08 06:16:17"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-08 10:35:01"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514899310542848",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-09 04:18:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247270257133268992",
            "tweetText" : "Revolutionieren Sie Ihren Unterricht mit Microsoft Surface und Workplace as a Service von ALSO. Wie sparen andere Bildungseinrichtungen Geld, Zeit und Stress mit WaaS? Finden Sie es hier heraus.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Business-IT",
            "screenName" : "@bITexperts_DE"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UZH_ch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniBasel_en"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UniLuzern"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@phd_UniBasel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@inf_unibe"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@IntOfficeUnibas"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HydroUniBern"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kriPoUZH"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@sub_unibe"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 04:22:51"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245267569755463681",
            "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
            "urls" : [ "https://t.co/I4grLgGEqj" ],
            "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Datenschutz"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:58:30"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "SGKB News",
            "screenName" : "@sgkb_news"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ReutersBiz"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FortuneMagazine"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Entrepreneur"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tagesanzeiger"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@businessinsider"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TEDTalks"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MariaBartiromo"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MarketWatch"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SRF"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:11:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610426494978",
            "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:26:25"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1248068111892447243",
            "tweetText" : "The 120Hz refresh rate was a key breakthrough in #OPPOFindX2Series' Ultra Vision Screen, but also a piece in a bigger puzzle. 🔬\n\nClick below and dive into the full story. #FindMore 📱",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "OPPO",
            "screenName" : "@oppo"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Android"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ForbesTech"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AndroidAuth"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MKBHD"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AndroidDev"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@engadget"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@androidcentral"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 49"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:35:50"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Orchid",
            "screenName" : "@OrchidProtocol"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@EFF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@AndroidDev"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 and up"
          } ],
          "impressionTime" : "2020-04-09 03:11:16"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247810756118224896",
            "tweetText" : "#Webinar am 15. April 2020, 13:00 - 13:30 Uhr: Vom Prozess zum Workflow! Automatisieren von Prozessen in der Nintex Workflow Cloud. \n\nJetzt anmelden!\n#ProcessExcellence #BusinessProcess #ProcessAutomation",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "NLA",
            "screenName" : "@NLA_Processes"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 03:17:20"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514947989671937",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-09 03:17:05"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514970320207872",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-09 07:45:42"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@federalreserve"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Investors and patents"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business and finance news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business & finance"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 07:45:40"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247847452738236416",
            "tweetText" : "Was bei der Erstellung eines Webshops zu beachten ist, haben wir in einer Kurzanleitung für Sie zusammengefasst.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "kmu"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 11:20:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514844566597635",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-09 11:20:47"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610426494978",
            "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-09 11:19:59"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240578761495425040",
            "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:22:06"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240158044413202434",
            "tweetText" : "【News】20,000+ Components in Seeed's OPL to Save Your Time and Money on Your PCB Assembly Order! Learn More 👉 https://t.co/qrSj1FOTtt https://t.co/gX8tJ8frrK",
            "urls" : [ "https://t.co/qrSj1FOTtt" ],
            "mediaUrls" : [ "https://t.co/gX8tJ8frrK" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Seeed Studio",
            "screenName" : "@seeedstudio"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@sparkfun"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@hackaday"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@arduino"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@adafruit"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:40:30"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1225134285335609345",
            "tweetText" : "type Query {\n  isGraphQL: FaunaDB!\n}\n\nTry FaunaDB: https://t.co/XqAAsPPPyv",
            "urls" : [ "https://t.co/XqAAsPPPyv" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Fauna",
            "screenName" : "@fauna"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@reactjs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@JavaScriptDaily"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@angular"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:27:48"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1248267112730234880",
            "tweetText" : "Arbeiten Mitarbeitende im #Homeoffice produktiver als im Büro? Mit den passenden Rahmenbedingungen und Tätigkeiten, ja!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom Business",
            "screenName" : "@Swisscom_B2B_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@derspiegel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@spiegelonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@faznet"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@NZZ"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@TEDTalks"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Forbes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@zeitonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@SZ"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "work"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:27:45"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1208115770225377280",
            "tweetText" : "Security experts across all industries face the same challenge: how do I improve defenses against 🤖bot-generated traffic? 🤔\n\nIn our new #ebook, we take a look at ways attackers employ bots &amp; how your company can guard against bot attacks. Download --&gt; https://t.co/2Ldqmwri4G https://t.co/TfGV5HIHcI",
            "urls" : [ "https://t.co/2Ldqmwri4G" ],
            "mediaUrls" : [ "https://t.co/TfGV5HIHcI" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Signal Sciences",
            "screenName" : "@signalsciences"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@owasp"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@HackingDave"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@joshcorman"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@WeldPond"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jack_daniel"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@rapid7"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@DarkReading"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@thegrugq"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gattaca"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@metasploit"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jeremiahg"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@k8em0"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@securityweekly"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:33:59"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1093107212556345344",
            "tweetText" : "Private virtual Platform as a Service for developers.\nLots of features available from the box.\nTry for free!\n https://t.co/6cuN6zzRhI \n#d2cio #docker #4devs https://t.co/37UxQaNQxg",
            "urls" : [ "https://t.co/6cuN6zzRhI" ],
            "mediaUrls" : [ "https://t.co/37UxQaNQxg" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "D2C.io",
            "screenName" : "@d2cio"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Azure"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ThePSF"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MongoDB"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GCPcloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ubuntu"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Linux"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@debian"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@codinghorror"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "devops"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "linux"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "golang"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "javascript"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "python"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#golang"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "13 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:40:24"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1248195078352506881",
            "tweetText" : "FREE Uptime Monitor &amp; Server Resource Usage Monitor\n\nGet alerts if your websites/servers go down, or if they start using too many resources\n\nAlerts via: Email SMS Telegram PushBullet Pushover Twitter Slack Discord Mattermost RocketChat MicrosoftTeams PagerDuty OpsGenie VictorOps",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "HetrixTools",
            "screenName" : "@HetrixTools"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@zabbix"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:26:50"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247479734000406528",
            "tweetText" : "Welche Chancen haben Menschen ohne Zugang zu funktionierenden medizinischen und sanitären Einrichtungen im Kampf gegen #COVIDー19 ?\n\nGemeinsam müssen wir jenen helfen, die gefährdet sind.\n👉 https://t.co/SMynVT5eLg https://t.co/2L5gWypIVg",
            "urls" : [ "https://t.co/SMynVT5eLg" ],
            "mediaUrls" : [ "https://t.co/2L5gWypIVg" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "IKRK",
            "screenName" : "@IKRK"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ICRC"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RotesKreuz_CH"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:22:37"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1103588856354533376",
            "tweetText" : "We are excited to introduce Mind Maps by Miro: a simple yet smart new tool!  Capture, organize and structure ideas quickly without switching between tools. https://t.co/bXOCWRQ6rl https://t.co/P4rOKGNK14",
            "urls" : [ "https://t.co/bXOCWRQ6rl" ],
            "mediaUrls" : [ "https://t.co/P4rOKGNK14" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Business news and general info"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:26:44"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1247847388422815744",
            "tweetText" : "Im Moment kann ein Webshop sehr nützlich sein. Deshalb haben wir recherchiert und fassen in unserem Beitrag zusammen, wie Sie schnell und unbürokratisch zum eigenen Webshop gelangen.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "PostFinance",
            "screenName" : "@PostFinance"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "kmu"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:25:01"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1230998681026883584",
            "tweetText" : "Learn how to get started with #DevSecOps with this checklist.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Azure"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@kubernetesio"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@MongoDB"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlabstatus"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#logging"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:27:38"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245376969421848583",
            "tweetText" : "Es gibt viele Wege smart verbunden zu sein: Entdecke die Apple Watch mit Multi Device.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@tim_cook"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Apple"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Apple - iPhone"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Gender",
            "targetingValue" : "Men"
          } ],
          "impressionTime" : "2020-04-10 06:27:26"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237646416677408768",
            "tweetText" : "Sie verstehen nichts von Hypotheken? Wir beraten Sie persönlich und kostenlos. #valuu",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Valuu",
            "screenName" : "@valuuapp"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Retargeting campaign engager",
            "targetingValue" : "Retargeting campaign engager: 23109800"
          }, {
            "targetingType" : "Retargeting engagement type",
            "targetingValue" : "Retargeting engagement type: 1"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "35 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:25:22"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1181307044550381569",
            "tweetText" : "Correlate logs with metrics and traces, easily investigate trends with the Log Patterns view, and now, re-index archived logs to provide historical context. Learn more here: https://t.co/MnoLgq4YuK",
            "urls" : [ "https://t.co/MnoLgq4YuK" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Datadog, Inc.",
            "screenName" : "@datadoghq"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@rapid7"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DevOps"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#DevOps"
          }, {
            "targetingType" : "Tailored audiences (lists)",
            "targetingValue" : "AWS Customers v2"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-10 06:40:14"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Huawei Europe",
            "screenName" : "@Huawei_Europe"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 06:22:05"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Android",
            "deviceId" : "JgBihTt9AJY/IOC1OE+ZBFGAoCCQZC8exjS86CcDygs=",
            "deviceType" : "Samsung Galaxy Note III"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1160922800854577153",
            "tweetText" : "Intelligence Artificielle, Jumeaux Numériques, IoT... Réinventons le futur de demain !",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Siemens Digital Industries Software France",
            "screenName" : "@siemensSW_FR"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "AI"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tech"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#data"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#AI"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#bigdata"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "data"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-10 14:56:04"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237709976635916288",
            "tweetText" : "Finden Sie den Blick-Käfer? Jetzt mitmachen und Preise im Gesamtwert von über CHF 15'000.– gewinnen!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "18 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 14:39:00"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610426494978",
            "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-10 14:38:59"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514970320207872",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-24 07:29:34"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245267569755463681",
            "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
            "urls" : [ "https://t.co/I4grLgGEqj" ],
            "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Datenschutz"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-24 07:29:55"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "VIAVI Enterprise",
            "screenName" : "@ViaviEnterprise"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BlackHatEvents"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-24 07:26:54"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Android",
            "deviceId" : "JgBihTt9AJY/IOC1OE+ZBFGAoCCQZC8exjS86CcDygs=",
            "deviceType" : "Samsung Galaxy Note III"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237001353857323013",
            "tweetText" : "Follow us for a new groundbreaking cybersecurity research. @cybernews_com 🔒",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CyberNews",
            "screenName" : "@CyberNews_com"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Tech news"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "social media"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#machinelearning"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "tech"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "wireless"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#webdesign"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#iot"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "samsung"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "html"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#bigdata"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#samsung"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "microsoft"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#computer"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#software"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "windows"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#tech"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "computer"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "android"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#android"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#technews"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "software"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "gamification"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#gamification"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "iot"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "laptop"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#html"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-11 10:31:30"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-27 20:18:00"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "SearchTweets",
          "promotedTweetInfo" : {
            "tweetId" : "1242885285454675968",
            "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-27 20:18:09"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1234474830713511936",
            "tweetText" : "Jelastic Cloud by Infomaniak:\nDie professionelle PaaS-Lösung, um Ihre Web-Apps\nin der Cloud zu erstellen, zu testen und einzusetzen ☁ 🚀\n\nJetzt 14 Tage kostenlos testen\n➡️ https://t.co/HmF26CGpd3 https://t.co/nsZweoy1cU",
            "urls" : [ "https://t.co/HmF26CGpd3" ],
            "mediaUrls" : [ "https://t.co/nsZweoy1cU" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-27 20:19:22"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-27 15:55:55"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@BW"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FT"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business & finance"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Investors and patents"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Business and finance news"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-27 15:55:00"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243254498275049472",
            "tweetText" : "Ungenutzte Chancen, grosses Potenzial: Wie können wir Jugendliche vermehrt für Mathematik, Informatik, Naturwissenschaften und Technik (MINT) begeistern? Ein Interview mit René Hüsler über Kreativität, Koordination und Standhaftigkeit.   #hsluinformatik #mint #informatik",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Hochschule Luzern",
            "screenName" : "@hslu"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Informatik"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-03-28 00:09:48"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-28 11:44:26"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1233321174257545216",
            "tweetText" : "kDrive: Die neue cloudbasierte Collaboration-Lösung von Infomaniak 🇨🇭\n\nJetzt 30 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/Lv1NdYJA99 https://t.co/4vFjNSeYVr",
            "urls" : [ "https://t.co/Lv1NdYJA99" ],
            "mediaUrls" : [ "https://t.co/4vFjNSeYVr" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#privacy"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "hostpoint"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "privacy"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "cloud computing"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-28 11:44:27"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1233320353201520640",
            "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr!\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat\n\n Jetzt 30 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/Lv1NdYJA99 https://t.co/pjIKQuCalk",
            "urls" : [ "https://t.co/Lv1NdYJA99" ],
            "mediaUrls" : [ "https://t.co/pjIKQuCalk" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "privacy"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Datenschutz"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#privacy"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-28 13:29:08"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1214887913865056259",
            "tweetText" : "Lesen Sie im E-book, wie die Entwicklung von Cloud-nativen Anwendungen durch #DevOps beschleunigt und verbessert werden kann. #Cloud #IBM",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "IBM Deutschland",
            "screenName" : "@IBMDeutschland"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@RedHat"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-03-28 18:19:34"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1234474830713511936",
            "tweetText" : "Jelastic Cloud by Infomaniak:\nDie professionelle PaaS-Lösung, um Ihre Web-Apps\nin der Cloud zu erstellen, zu testen und einzusetzen ☁ 🚀\n\nJetzt 14 Tage kostenlos testen\n➡️ https://t.co/HmF26CGpd3 https://t.co/nsZweoy1cU",
            "urls" : [ "https://t.co/HmF26CGpd3" ],
            "mediaUrls" : [ "https://t.co/nsZweoy1cU" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-28 18:20:23"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243568468978532352",
            "tweetText" : "In der aktuellen Situation stehen die Märkte stark unter Druck. Daniel Kalt, UBS-Chefökonom Schweiz, gibt einen Finanz- und Wirtschaftsausblick und geht im Webcast auf die derzeit wichtigsten Ereignisse ein. Jetzt Webcast anschauen: https://t.co/wfXnS6OEsl #shareUBS https://t.co/zq5U7KmfX5",
            "urls" : [ "https://t.co/wfXnS6OEsl" ],
            "mediaUrls" : [ "https://t.co/zq5U7KmfX5" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "UBS Schweiz",
            "screenName" : "@UBSschweiz"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "KMU"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Wirtschaft"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-03-28 18:18:36"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 03:17:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610359283719",
            "tweetText" : "Keine Lust auf sprunghafte Berater? Nimm deine Säule 3a selbst in die Hand. #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 03:28:42"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242885285454675968",
            "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 03:17:13"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245267569755463681",
            "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
            "urls" : [ "https://t.co/I4grLgGEqj" ],
            "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Dropbox"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@googledrive"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Datenschutz"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 08:47:54"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 08:53:36"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610426494978",
            "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 08:39:29"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237645336203624449",
            "tweetText" : "Unsere Hypothekarexperten verkuppeln Sie mit Ihrer Traum-Hypothek. #valuu",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Valuu",
            "screenName" : "@valuuapp"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 08:39:47"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242877650500190209",
            "tweetText" : "To run with the bulls, you have to be fast. With five years of giving Europeans the easiest trading experience available, we have developed an intuitive, all-in-one investing tool with access to 30+ digital assets including #bitcoin, gold and more.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Bitpanda",
            "screenName" : "@bitpanda"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@ExpansionMx"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@lajornadaonline"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@VitalikButerin"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "crypto"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "technology"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "cybersecurity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "mining"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#cybersecurity"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#crypto"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-01 08:41:19"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242885285454675968",
            "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 08:38:29"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 08:34:56"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514899310542848",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-01 08:34:57"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1052607373263556608",
            "tweetText" : "The DevOps Roadmap for Security eBook: Tips and tools for bridging the security tribe into DevOps.\n\nDownload today --&gt; https://t.co/YitOkx9x7X https://t.co/gHoZkOq1TZ",
            "urls" : [ "https://t.co/YitOkx9x7X" ],
            "mediaUrls" : [ "https://t.co/gHoZkOq1TZ" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Signal Sciences",
            "screenName" : "@signalsciences"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 04:05:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "Lombard Odier",
            "screenName" : "@lombardodier"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@GoldmanSachs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@CreditSuisse"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@jpmorgan"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@FinancialTimes"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@UBS"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 04:06:19"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514970320207872",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-01 18:00:05"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242885285454675968",
            "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 15:50:16"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240578761495425040",
            "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 15:48:58"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244696969165094912",
            "tweetText" : "Lerne effektive Architekturen für große Angular-Anwendungen zu entwerfen!",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Angular College",
            "screenName" : "@AngularArc"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "java"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 15:50:31"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1227090969994125312",
            "tweetText" : "Miro combines all the necessary pieces in one place and lets your team collaborate beyond formats, tools, notifications, locations and time zones. Try now! https://t.co/q8LFi613tr https://t.co/kNegAngzYK",
            "urls" : [ "https://t.co/q8LFi613tr" ],
            "mediaUrls" : [ "https://t.co/kNegAngzYK" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Microsoft Office"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 49"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-01 15:50:08"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237645839138525184",
            "tweetText" : "In Immobilien investieren? Valuu verkuppelt Sie mit der besten Hypothek dazu. #valuu",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Valuu",
            "screenName" : "@valuuapp"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 15:50:13"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "VMware DACH",
            "screenName" : "@vmware_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cloud computing"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cybersecurity"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "Cisco"
          }, {
            "targetingType" : "Conversation topics"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 15:48:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1230998681026883584",
            "tweetText" : "Learn how to get started with #DevSecOps with this checklist.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlabstatus"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#logging"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "gitlab"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-01 15:50:20"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514899310542848",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-01 15:49:48"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1234474705572257795",
            "tweetText" : "Jelastic Cloud by Infomaniak:\nDie professionelle PaaS-Lösung, um Ihre Web-Apps\nin der Cloud zu erstellen, zu testen und einzusetzen ☁ 🚀\n\nJetzt 14 Tage kostenlos testen\n➡️ https://t.co/HmF26CGpd3 https://t.co/O15SJ9jby1",
            "urls" : [ "https://t.co/HmF26CGpd3" ],
            "mediaUrls" : [ "https://t.co/O15SJ9jby1" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Infomaniak",
            "screenName" : "@infomaniak"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Tailored audiences (web)"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "account signup"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "purchase"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 17:59:40"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610426494978",
            "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-01 17:57:49"
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adImpressions" : {
        "impressions" : [ {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1245396122916589575",
            "tweetText" : "Hilfe organisieren oder Hilfe bekommen.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Intergeneration",
            "screenName" : "@intergenerativ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 00:42:16"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240676412316684293",
            "tweetText" : "CloudBees Rollout Tutorial: Feature Flagging in your React Native App in 5 minutes https://t.co/655Gzszlto",
            "urls" : [ "https://t.co/655Gzszlto" ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "CloudBees",
            "screenName" : "@CloudBees"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "#webdevelopment"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#programming"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#webdev"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-02 00:39:55"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1242874852836589575",
            "tweetText" : "Für eure Service-Anliegen sind die Swisscom Shops an den wichtigsten Standorten von Mo–Sa von 10 bis 15 Uhr geöffnet.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Swisscom",
            "screenName" : "@Swisscom_de"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 00:32:20"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1222927367745736709",
            "tweetText" : "Start collecting enhanced AWS Lambda metrics in real-time with Datadog and begin tracking the performance of your serverless functions seconds after they've been invoked. Start a free trial today:",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Datadog, Inc.",
            "screenName" : "@datadoghq"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Computer programming"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Enterprise software"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Open source"
          }, {
            "targetingType" : "Interests",
            "targetingValue" : "Startups"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DevOps"
          }, {
            "targetingType" : "Tailored audiences (lists)",
            "targetingValue" : "AWS Customers v2"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-02 00:39:42"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1240631610426494978",
            "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "frankly.",
            "screenName" : "@frankly_zkb"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 00:34:53"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "WtfSidebar",
          "advertiserInfo" : {
            "advertiserName" : "AJC Berlin",
            "screenName" : "@AJCBerlin"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Iran"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "21 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 00:32:18"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1237645839138525184",
            "tweetText" : "In Immobilien investieren? Valuu verkuppelt Sie mit der besten Hypothek dazu. #valuu",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Valuu",
            "screenName" : "@valuuapp"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 00:35:29"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243235262101983232",
            "tweetText" : "Learn how you can improve the security of your infrastructure and applications with the DevOps security checklist. ✅ #DevOps #DevSecOps",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Sqreen",
            "screenName" : "@SqreenIO"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@github"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@nodejs"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlabstatus"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@gitlab"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@awscloud"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@linuxfoundation"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@Docker"
          }, {
            "targetingType" : "Conversation topics",
            "targetingValue" : "DevOps"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "#logging"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "github"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "gitlab"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 and up"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-02 00:39:04"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1243185750440583168",
            "tweetText" : "Strom klug nutzen dank intelligenter Stromnetze: Ein Forschungsteam der Hochschule Luzern – Informatik hat dazu neuartige Lösungsansätze entwickelt. #hsluinformatik #smartgrid #informatik #predictiveanalytics",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Hochschule Luzern",
            "screenName" : "@hslu"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Keywords",
            "targetingValue" : "Strom"
          }, {
            "targetingType" : "Keywords",
            "targetingValue" : "Informatik"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-02 00:50:12"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1052607373263556608",
            "tweetText" : "The DevOps Roadmap for Security eBook: Tips and tools for bridging the security tribe into DevOps.\n\nDownload today --&gt; https://t.co/YitOkx9x7X https://t.co/gHoZkOq1TZ",
            "urls" : [ "https://t.co/YitOkx9x7X" ],
            "mediaUrls" : [ "https://t.co/gHoZkOq1TZ" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Signal Sciences",
            "screenName" : "@signalsciences"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@dakami"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@thegrugq"
          }, {
            "targetingType" : "Follower look-alikes",
            "targetingValue" : "@defcon"
          }, {
            "targetingType" : "Age",
            "targetingValue" : "25 to 54"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 00:38:54"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1204325079468953600",
            "tweetText" : "Create a hub for cross-functional work that also works with all your other tools. Centralize and standardize communication with Miro.  https://t.co/lxZbwSfLLE https://t.co/9SLf0Y94Na",
            "urls" : [ "https://t.co/lxZbwSfLLE" ],
            "mediaUrls" : [ "https://t.co/9SLf0Y94Na" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Miro",
            "screenName" : "@MiroHQ"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Interests",
            "targetingValue" : "Enterprise software"
          }, {
            "targetingType" : "Tailored audiences (web)",
            "targetingValue" : "RealtimeBoard users"
          }, {
            "targetingType" : "Languages",
            "targetingValue" : "English"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-02 00:38:57"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244630934231580673",
            "tweetText" : "@kariem_hussein hält euch mit Videos über mentale Stärke, Ausdauer und Wohlbefinden fit. Alles dazu findet ihr in unserem Blog ➡️ https://t.co/oBrSXE6Zai \n#soschützenwiruns #bleibtzuhause #stayathome #sport #running #motivation #prävention #laufsport #laufen #groupemutuel https://t.co/FSpLLNiXoR",
            "urls" : [ "https://t.co/oBrSXE6Zai" ],
            "mediaUrls" : [ "https://t.co/FSpLLNiXoR" ]
          },
          "advertiserInfo" : {
            "advertiserName" : "Groupe Mutuel",
            "screenName" : "@Groupe_Mutuel"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          } ],
          "impressionTime" : "2020-04-02 00:42:47"
        }, {
          "deviceInfo" : {
            "osType" : "Desktop"
          },
          "displayLocation" : "TimelineHome",
          "promotedTweetInfo" : {
            "tweetId" : "1244514970320207872",
            "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
            "urls" : [ ],
            "mediaUrls" : [ ]
          },
          "advertiserInfo" : {
            "advertiserName" : "BLICK",
            "screenName" : "@Blickch"
          },
          "matchedTargetingCriteria" : [ {
            "targetingType" : "Languages",
            "targetingValue" : "German"
          }, {
            "targetingType" : "Locations",
            "targetingValue" : "Switzerland"
          }, {
            "targetingType" : "Platforms",
            "targetingValue" : "Desktop"
          } ],
          "impressionTime" : "2020-04-02 00:50:20"
        } ]
      }
    }
  }
} ]